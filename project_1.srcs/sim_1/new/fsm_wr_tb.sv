
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/01/2020 05:10:17 PM
// Design Name: 
// Module Name: fsm_rd_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fsm_wr_tb();
   timeunit 1ns;
   timeprecision 100ps;   

`define PERIOD 10

   logic clk = 1'b0;
   logic rst = 1'b1;
   
   logic control_i = 1'b0;
   logic [15:0] data_i;
   logic [15:0] data_o;
   logic [2:0] 	addr_o;
   logic 		control_we_o;
   logic 		control_fsm_o;
   
   
   clk_rst_if bus();
   assign bus.clk = clk;
   assign bus.rst = rst;
   
   always
	 begin : CLK_PROCESS
		#(`PERIOD/2);
		clk = ~clk;
	 end : CLK_PROCESS


   // Signal monitor
   initial
	 begin
		$timeformat ( -9, 0, "ns", 3 ) ;
		$monitor ( "%t\tcontrol_i=%b, state=%0s, addr=%d",
				   $time, control_i, fsm_wr_inst.fsm_state.name(),
				   addr_o);
		
		#(`PERIOD * 99)			// wait for 99 * 10 ns
		$display ( "TEST TIMEOUT" );
		$finish;					// Stops testbench
	 end
   
   fsm_wr fsm_wr_inst
	 (
	  .clk_rstn_intrf(bus),
	  .control_i(control_i),
	  .data_i(data_i),	// Median value from filter
	  .data_o(data_o),	// Output value for BRAM
	  .addr_o(addr_o),	// RAM Address to write to
	  .control_we_o(control_we_o),	// Write enable for RAM
	  .control_fsm_o(control_fsm_o)		// Signal for fsm_rd to start
	  );

   initial 
	 begin: TB
		rst = 1'b0;
		control_i = 1'b0;
		data_i = 16'd0;
		@(posedge clk);
		rst = 1'b1;
		data_i = 16'd0;	// Write data without data available signal		
		
		#(`PERIOD);

		control_i = 1'b1;		// Enable data available signal
		#(`PERIOD);
		control_i = 1'b0;		// Disable data available signal
		#(`PERIOD*4);


		control_i = 1'b1;		// Enable data available signal		
		data_i = 16'd1;		
		#(`PERIOD);
		control_i = 1'b0;		// Enable data available signal		
		#(`PERIOD);
		
		for(int i = 2; i<10; i++)
		  begin
			 control_i = 1'b1;		// Enable data available signal 
			 data_i = i;
			 #(`PERIOD);
			 control_i = 1'b0;		// Enable data available signal
			 #(`PERIOD);
		  end
		
		#(`PERIOD);
		$display("ALL TESTS PASSED");
		$finish();
	 end: TB

endmodule
