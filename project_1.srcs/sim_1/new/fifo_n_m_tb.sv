//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/25/2020 12:35:30 PM
// Design Name: 
// Module Name: fifo_n_m_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fifo_n_m_tb();
   timeunit 1ns;
   timeprecision 100ps;

   localparam DATA_WIDTH = 16;
   // localparam DEPTH = 3;

   logic clk_i = 1'b1;
   logic rst_i = 1'b1;
   logic [DATA_WIDTH-1:0]data_i = 0;
   logic [DATA_WIDTH-1:0]data0_o;
   logic [DATA_WIDTH-1:0]data1_o;
   logic [DATA_WIDTH-1:0]data2_o;
   logic write_en_i=1'b0;
   // logic read_en_i = 1'b0;
   clk_rst_if bus();
   logic full_o;
   logic empty_o;
   
   assign bus.clk = clk_i;
   assign bus.rst = rst_i;

   // CLK definition
`define PERIOD 10

   always
	 begin : CLK_PROCESS
		#(`PERIOD/2) clk_i = ~clk_i;		
	 end : CLK_PROCESS

   // FIFO instance
   fifo_n_m #(
			  .DATA_WIDTH(DATA_WIDTH)
			  // .DEPTH(DEPTH)
			  )
   fifo_8_3_inst
	 (
	  .data_i(data_i),
	  .clk_rstn_intrf(bus),
	  // .read_en_i(read_en_i),
	  .write_en_i(write_en_i),
	  .data0_o(data0_o),
	  .data1_o(data1_o),
	  .data2_o(data2_o),
	  .full_o(full_o),
	  .empty_o(empty_o)
	  );

   // Monitor Results
   initial 
	 begin
		$timeformat ( -9, 0, "ns", 3 );
		$monitor ( "%t w_en=%b data_i=0x%h data0_o=0x%h data1_o=0x%h data2_o=0x%h",
				   $time,   write_en_i,   data_i, data0_o, data1_o, data2_o ) ;
	 end

   
   initial
	 begin : TB
		#100;					// 100 ns
		@(posedge clk_i)
		  begin
			 rst_i = 1'b0;
		  end
		@(posedge clk_i)
		  begin
			 rst_i = 1'b1;
		  end
		
		@(posedge clk_i)
		  begin
			 rst_i = 1'b1;
		  end		
		
		#(`PERIOD)				// Wait for one period

		// Read a value
		// Should not change output,
		// since empty_o is 1
		// @(posedge clk_i)
		  // begin
			 // read_en_i = 1'b1;
		  // end

		// Write 1
		@(posedge clk_i)
		  begin
			 // read_en_i = 1'b0;
			 write_en_i = 1'b1;
			 data_i = 8'h01;
		  end

		// Write 2 
		@(posedge clk_i)
		  begin
			 data_i = 8'h02;
		  end

		// Read 1
		@(posedge clk_i)
		  begin
			 write_en_i = 1'b0;
			 // read_en_i = 1'b1;
		  end

		// Write 3
		@(posedge clk_i)
		  begin
			 // read_en_i = 1'b0;			 			 
			 write_en_i = 1'b1;			 
			 data_i = 8'h03;
		  end

		// Read 2
		@(posedge clk_i)
		  begin
			 write_en_i = 1'b0;
			 // read_en_i = 1'b1;
		  end

		// Write 4
		@(posedge clk_i)
		  begin
			 // read_en_i = 1'b0;			 			 
			 write_en_i = 1'b1;			 
			 data_i = 8'h04;
		  end

		// Write 5
		@(posedge clk_i)
		  begin
			 data_i = 8'h05;
		  end

		// Write 6
		@(posedge clk_i)
		  begin
			 data_i = 8'h06;
		  end

		// Read 3
		@(posedge clk_i)
		  begin
			 // read_en_i = 1'b1;
			 write_en_i = 1'b0;
		  end


		// #(`PERIOD * 3);			// Empty FIFO

		$display("ALL TESTS PASSED");
		$finish(0);
	 end : TB
   
   
endmodule
