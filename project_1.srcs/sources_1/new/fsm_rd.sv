`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/31/2020 03:07:45 PM
// Design Name: 
// Module Name: fsm_wr
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: An FSM which handles reading 8 values from a RAM,
// and calculating their average
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fsm_rd
  (
   clk_rst_if.slave clk_rstn_intrf,
   input logic control_i,	  // Signal that the FSM is clear to start
   input logic [15:0] rd_data_i, // Data from RAM, 16bit
   output logic [2:0] addr_o,	 // RAM address to read from
   output logic [15:0] avg_o	 // Final average result
   );

   // Typedef for FSM states
   typedef enum 	   logic [2:0] {IDLE, READING, INCREMENT, WAIT_MEM0, WAIT_MEM1, DIVIDE, WAITING} state_t;
   state_t fsm_state;

   logic [18:0] 	   accumulator; // Internal accumulation of 8 x 16bit values,
								// with 3 bits extra length to prevent overflow
   

   always_ff @(posedge clk_rstn_intrf.clk or negedge clk_rstn_intrf.rst)
	 begin : FSM
		if(~clk_rstn_intrf.rst)
		  begin : RESET
			 fsm_state <= IDLE;
			 addr_o <= 3'd0;
			 avg_o <= 16'd0;
			 accumulator <= 19'd0;
		  end : RESET
		else 					// posedge of CLK
		  begin : STATES
			 case(fsm_state)
			   // Initial state, which idles waiting
			   // for a control signal
			   IDLE:
				 begin
					if(control_i)
					  begin
						 fsm_state <= READING;						 
					  end
					else
					  begin
						 fsm_state <= IDLE;
					  end
				 end // case: IDLE

			   // State which reads a value from RAM using current
			   // address and addring it to the internal accumulator
			   READING:
				 begin
					accumulator <= accumulator + rd_data_i; // Read data from RAM
					addr_o <= addr_o + 1;
					if(addr_o >= 3'd7) // Read all BRAM
					  begin
						 fsm_state <= WAITING;
					  end
					else
					  begin
						 fsm_state <= WAIT_MEM0;
					  end
				 end // case: READING
			   
			   WAIT_MEM0:
			   	 begin
			   		fsm_state <= WAIT_MEM1;
			   	 end
			   
			   WAIT_MEM1:
				 begin
					fsm_state <= READING;
				 end			   			   
			   
			   // Final state, which loops forever
			   WAITING:
				 begin
					avg_o <= (accumulator >> 3);
					fsm_state <= WAITING;
				 end
			   
			 endcase
		  end : STATES
	 end : FSM
endmodule
