// `timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/25/2020 11:59:47 AM
// Design Name: 
// Module Name: fifo_n_m
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// Simple configurable FIFO
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// Adapted version of http://www.asic-world.com/examples/systemverilog/syn_fifo.html
//////////////////////////////////////////////////////////////////////////////////
module fifo_n_m
  #(DATA_WIDTH=16, DEPTH=3)
   (
	input logic [DATA_WIDTH-1:0]  data_i,
								  clk_rst_if.slave clk_rstn_intrf, // CLK and RST interface in
	input logic 				  read_en_i, // Read enable signal
	input logic 				  write_en_i, // Write enable signal
	output logic [DATA_WIDTH-1:0] data_o , // Data out
	output logic 				  full_o, // FIFO full
	output logic 				  empty_o				  // FIFO empty
    );
   
   // Internal variables
   logic [DATA_WIDTH-1:0] 		  mem [0:DEPTH-1]; // Internal memory for FIFO
   logic [DEPTH-1:0] 			  items_count = 0;	   // Total number of elements stored
   logic [DEPTH-1:0] 			  write_index = 0; // Last write address
   logic [DEPTH-1:0] 			  read_index = 0;  // Last read adress

   // Items count checker
   always_comb
	 begin : COUNT_ITEMS
		if(items_count == DEPTH)
		  begin
			 full_o = 1'b1;
		  end
		else
		  begin
			 full_o = 1'b0;
		  end
		if(items_count == 0)
		  begin
			 empty_o = 1'b1;
		  end
		else
		  begin
			 empty_o = 1'b0;
		  end
	 end : COUNT_ITEMS

   always_ff @ (posedge clk_rstn_intrf.clk, negedge clk_rstn_intrf.rst)
	 begin
		if(!clk_rstn_intrf.rst)
		  begin : RESET			// Reset signal received
			 for (int i = 0; i < DEPTH; i++)
			   begin : FIFO_CLEAR
				  mem[i] = 0;
			   end : FIFO_CLEAR
			 write_index = 0;
			 read_index = 0;
			 items_count = 0;
		  end : RESET
		else
		  begin
			 if(read_en_i && !empty_o)
			   begin : READ_DATA
				  data_o = mem[read_index];
				  read_index = (read_index + 1) % DEPTH; // Wrap around, not sure if efficient
				  items_count -= 1;
			   end : READ_DATA
			 else if(write_en_i && !full_o)
			   begin : WRITE_DATA
				  mem[write_index] = data_i;
				  write_index = (write_index + 1) % DEPTH;
				  items_count += 1;
			   end : WRITE_DATA
		  end
	 end // always_ff @ (posedge clk_rstn_intrf.clk, negedge clk_rstn_intrf.rst)

   // Concurrent assertions
   property full_check;
	  @(posedge clk_rstn_intrf.clk) disable iff (!clk_rstn_intrf.rst)
		items_count == DEPTH |-> (full_o && ~empty_o);
   endproperty // full_check
   
   full_check_assert : assert property (full_check);
   
   property empty_check;
	  @(posedge clk_rstn_intrf.clk) disable iff (!clk_rstn_intrf.rst)
		items_count == 0 |-> (empty_o && ~full_o);
   endproperty // empty_check

   empty_check_assert : assert property (empty_check);

   // Make sure that data inserted on previous cycle was stored
   // Write index is taken from 2 ticks back, due to the fact that
   // it changes at the same time with data_i.
   property store_check;
	  @(posedge clk_rstn_intrf.clk) disable iff (!clk_rstn_intrf.rst)
		(write_en_i && ~full_o) |=> ($past(data_i) == mem[($past(write_index, 2))]);
		   // $display("Past data \"%h\", past index \"%d\"", $past(data_i), $past(write_index));
   endproperty // store_check

   store_check_assert : assert property (store_check);
   
endmodule
