// `timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/02/2020 08:15:28 PM
// Design Name: 
// Module Name: median_filter_3_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module median_filter_3_tb(

    );

   `timescale 1ns / 100ps   
   logic clk = 1'b0;
   logic rst = 1'b1;
   logic [15:0] data_i = 0;
   logic [15:0] data_o = 0;
   logic 		data_av_i = 1'b0;
   logic 		control_o;
   
   clk_rst_if bus();
   
   assign bus.clk = clk;
   assign bus.rst = rst;

`define PERIOD 10

   always
	 begin : CLK_PROCESS
		#(`PERIOD/2) clk = ~clk;		
	 end : CLK_PROCESS   

   median_filter_3 
	 #(.WIDTH(16))
	   median_inst 
		 (
		  .clk_rstn_intrf(bus),
		  .data_av_i(data_av_i),
		  .data_i(data_i),
		  .data_o(data_o),
		  .control_o(control_o)
		  );

   initial
	 begin: TB

		rst = 1'b0;
		#(`PERIOD);
		rst = 1'b1;
		@(posedge clk)
		  begin
			 data_i = 16'd1500;			 
		  end
		
		@(posedge clk)
		  begin
			 data_av_i = 1'b1;
		  end

		@(posedge clk)
		  begin
			 data_i = 16'd100;			 
		  end

		@(posedge clk)
		  begin
			 data_i = 16'd10;			 
		  end

		@(posedge clk)
		  begin
			 data_i = 16'd40000;			 
		  end
		
		@(posedge clk)
		  begin
			 data_i = 16'd300;			 
		  end

		@(posedge clk)
		  begin
			 data_i = 16'd1100;			 
		  end
		
		@(posedge clk)
		  begin
			 data_i = 16'd35000;			 
		  end

		@(posedge clk)
		  begin
			 data_i = 16'd2000;			 
		  end										
		
		#(`PERIOD);
		
		$finish(2);
		end : TB
endmodule
