// `timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/24/2020 07:55:37 PM
// Design Name: 
// Module Name: filter_daq
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module filter_daq
  (
   input logic [15:0]  data_i, // Data in from ADC
   input logic 		   data_av_ai, // Data available async in
   clk_rst_if.slave clk_rstn_intrf, // CLK and RST interface
   output logic [15:0] avg_o		// Data output
   );

   wire 			   clk_i;
   wire 			   rstn_i;
   wire 			   data_av_sync;
   
   logic [15:0] 	   median_i;
   logic 			   control_fsm_wr;
   logic 			   control_fsm_i;
   logic 			   control_we_i;
   logic [2:0] 		   bram_addra_i;
   logic [15:0] 	   wr_data;
   logic [15:0] 	   bram_doutb_i; // Data from RAM
   logic [2:0] 		   bram_addrb_i; // Read address for RAM
   
   sync_stage sync_stage_inst
	 (
	  .clk_rstn_intrf(clk_rstn_intrf),
	  .data_av_ai(data_av_ai),
	  .data_av_o(data_av_sync)
	  );

   median_filter_3 median_filter_inst
	 (
	  .clk_rstn_intrf(clk_rstn_intrf),
	  .data_av_i(data_av_sync),
	  .data_i(data_i),
	  .data_o(median_i),
	  .control_o(control_fsm_wr)
	  );


   
   // fsm_wr instance
   fsm_wr fsm_wr_inst
	 (
	  .clk_rstn_intrf(clk_rstn_intrf),
	  .control_i(control_fsm_wr),
	  .data_i(median_i),
	  .addr_o(bram_addra_i),
	  .data_o(wr_data),
	  .control_we_o(control_we_i),
	  .control_fsm_o(control_fsm_i)
	  );
   

   
   // BRAM instance
      blk_mem_gen_0 mem_inst
	 (
      .clka (clk_rstn_intrf.clk),  
      .wea  (control_we_i), 
      .addra(bram_addra_i),
      .dina (wr_data),
      .clkb (clk_rstn_intrf.clk),
      .addrb(bram_addrb_i),
      .doutb(bram_doutb_i)
	  );


   // fsm_rd instance
   fsm_rd fsm_rd_inst
	 (
	  .clk_rstn_intrf(clk_rstn_intrf),
	  .control_i(control_fsm_i),
	  .rd_data_i(bram_doutb_i),
	  .addr_o(bram_addrb_i),
	  .avg_o(avg_o)
	  );   

endmodule
