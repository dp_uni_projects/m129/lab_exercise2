//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/24/2020 09:52:01 PM
// Design Name: 
// Module Name: median_filter_3
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: A median filter which stores 3 values in a FIFO and 
// outputs the median value of the three.
// 
//////////////////////////////////////////////////////////////////////////////////

module median_filter_3
  #(WIDTH=16)
   (
	clk_rst_if.slave clk_rstn_intrf, // CLK and RST interface in
	input logic data_av_i,			 // New data available
	input logic [WIDTH-1:0] data_i,	 // New data in
	output logic [WIDTH-1:0] data_o, // Median value of stored values
	output logic control_o		// Signal FSM to store data to RAM
    );

   // Internal signals
   logic [WIDTH-1:0] data0_o; // FIFO data output
   logic [WIDTH-1:0] data1_o;
   logic [WIDTH-1:0] data2_o;
   logic 			 full_o;	// Read if FIFO is full
   
   
   // FIFO instance, width the same as data in of filter,
   // depth = 3 per the exercise's instructions.
   fifo_n_m
     #(
	   .DATA_WIDTH(WIDTH)
	   )
   fifo_8_3_inst
	 (
	  .data_i(data_i),				   // Data in
	  .clk_rstn_intrf(clk_rstn_intrf), // CLK and RST interface in
	  .write_en_i(data_av_i),	// Write enable signal
	  .data0_o(data0_o),		// Data out
	  .data1_o(data1_o),		// Data out
	  .data2_o(data2_o),		// Data out
	  .full_o(full_o)			// FIFO full
      );

   logic 			 has_data_in = 1'b0;
   // Output for FSM to store data.
   // Enabled only if the FIFO is full (i.e. we get a valid
   // median value) and new data has arrived
   // assign control_o = data_av_i;
   always_ff @(posedge clk_rstn_intrf.clk)
	 begin
		control_o <= data_av_i;
	 end
   
   // Simple combinational block which
   // performs two comparisons between the three
   // elements of the FIFO, outputting the median
   // value of the three.
   always_comb
	 begin : MEDIAN_VALUE
		if(data0_o > data1_o)
		  begin
			 if(data1_o > data2_o)
			   begin
				  data_o = data1_o;
			   end
			 else				// data0_o <= data2_o
			   begin
				  if(data0_o > data2_o)
					begin
					   data_o = data2_o;
					end
				  else
					begin
					   data_o = data0_o;
					end
			   end // else: !if(data1_o > data2_o)
		  end // if (data0_o > data1_o)
		else
		  begin
			 if(data1_o > data2_o)
			   begin
				  if(data0_o > data2_o)
					begin
					   data_o = data0_o;
					end
				  else
					begin
					   data_o = data2_o;
					end
			   end // if (data1_o > data2_o)
			 else
			   begin
				  data_o = data1_o;
			   end // else: !if(data1_o > data2_o)
		  end // else: !if(data0_o > data1_o)
		// end // if (full_o)
	 end : MEDIAN_VALUE
endmodule
