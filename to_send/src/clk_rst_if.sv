interface clk_rst_if;
   logic rst;
   logic clk;
   
   modport master (
					 output rst,
					 output clk
					 );
   
   modport slave (
				  input rst,
				  input clk
				  );
endinterface : clk_rst_if
