//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/24/2020 09:36:11 PM
// Design Name: 
// Module Name: sync_stage
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sync_stage
  (
   clk_rst_if.slave clk_rstn_intrf, // CLK and RST interface in
   input logic data_av_ai,
   output logic data_av_o
   );

   logic 		sync_stage_0_1;	// Connects the two registers
   // Sync stage 0
   register_n #(.WIDTH(1)) sync_stage_0
   	 (
   	  .data_i(data_av_ai),
   	  .clk_i(clk_rstn_intrf.clk),
   	  .rstn_i(clk_rstn_intrf.rst),
   	  .enable_i(1'b1),
   	  .data_o(sync_stage_0_1)
   	  );
   
   // Sync stage 1
   register_n #(.WIDTH(1)) sync_stage_1
   	 (
   	  .data_i(sync_stage_0_1),
   	  .clk_i(clk_rstn_intrf.clk),
   	  .rstn_i(clk_rstn_intrf.rst),
   	  .enable_i(1'b1),
   	  .data_o(data_av_o)
   	  );

   // // Two registers
   // logic 		s0;
   
   // always_ff @(posedge clk_rstn_intrf.clk, negedge clk_rstn_intrf.rst)
   // 	 begin
   // 		if(~clk_rstn_intrf.rst)
   // 		  begin
   // 			 s0 <= 1'b0;
   // 		  end
   // 		else
   // 		  begin
   // 			 s0 <= data_av_ai;
   // 			 data_av_o <= s0;
   // 		  end
   // 	 end
   
   
endmodule
