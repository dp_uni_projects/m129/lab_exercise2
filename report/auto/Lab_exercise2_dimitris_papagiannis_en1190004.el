(TeX-add-style-hook
 "Lab_exercise2_dimitris_papagiannis_en1190004"
 (lambda ()
   (setq TeX-command-extra-options
         "-output-directory=build")
   (TeX-run-style-hooks
    "uoa_template")
   (TeX-add-symbols
    "documentTitle")
   (LaTeX-add-labels
    "fig:overview"
    "fig:sync_stage_rtl"
    "fig:filter_schematic"
    "fig:fsm_wr_draw"
    "fig:fsm_rd_draw"
    "fig:tb_bram_read"
    "fig:tb_phase_01_marked"
    "fig:tb_phase_02_marked"
    "fig:tb_phase_03_marked"
    "fig:tb_sync_error"))
 :latex)

