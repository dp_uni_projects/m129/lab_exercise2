\contentsline {section}{\numberline {1}Εισαγωγή}{2}{section.1}%
\contentsline {subsection}{\numberline {1.1}Σκοπός}{2}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Αναμενόμενη συμπεριφορά}{2}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Δομή}{2}{subsection.1.3}%
\contentsline {section}{\numberline {2}Υλοποίηση}{3}{section.2}%
\contentsline {subsection}{\numberline {2.1}Synchronizer}{3}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Median filter}{4}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}RAM}{5}{subsection.2.3}%
\contentsline {subsection}{\numberline {2.4}FSM Write}{5}{subsection.2.4}%
\contentsline {subsection}{\numberline {2.5}FSM Read}{6}{subsection.2.5}%
\contentsline {section}{\numberline {3}Επαλήθευση}{8}{section.3}%
\contentsline {subsection}{\numberline {3.1}Assertions}{8}{subsection.3.1}%
\contentsline {subsubsection}{\numberline {3.1.1}Synchronizer}{8}{subsubsection.3.1.1}%
\contentsline {subsubsection}{\numberline {3.1.2}FSM Write}{8}{subsubsection.3.1.2}%
\contentsline {subsection}{\numberline {3.2}Testbench}{9}{subsection.3.2}%
\contentsline {subsubsection}{\numberline {3.2.1}Πρώτη και δεύτερη φάση --- 19 κύκλοι}{9}{subsubsection.3.2.1}%
\contentsline {subsubsection}{\numberline {3.2.2}Τρίτη φάση --- 24 κύκλοι}{10}{subsubsection.3.2.2}%
\contentsline {subsubsection}{\numberline {3.2.3}Παράλληλες διεργασίες}{11}{subsubsection.3.2.3}%
\contentsline {section}{\numberline {4}Ελλείψεις}{12}{section.4}%
