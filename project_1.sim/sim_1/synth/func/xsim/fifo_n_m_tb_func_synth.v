// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Mon Oct 26 19:05:03 2020
// Host        : mpliax-Inspiron-5593 running 64-bit Ubuntu 20.04.1 LTS
// Command     : write_verilog -mode funcsim -nolib -force -file
//               /home/mpliax/Sync/Documents/CTN_ENG/Classes/M119_MultiProcessing_on_IC/Lab/Lab_exercise2/project_1.sim/sim_1/synth/func/xsim/fifo_n_m_tb_func_synth.v
// Design      : fifo_n_m
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* DATA_WIDTH = "16" *) (* DEPTH = "3" *) 
(* NotValidForBitStream *)
module fifo_n_m
   (\clk_rstn_intrf\.rst ,
    \clk_rstn_intrf\.clk ,
    data_i,
    read_en_i,
    write_en_i,
    data_o,
    full_o,
    empty_o);
  input \clk_rstn_intrf\.rst ;
  input \clk_rstn_intrf\.clk ;
  input [15:0]data_i;
  input read_en_i;
  input write_en_i;
  output [15:0]data_o;
  output full_o;
  output empty_o;

  wire \clk_rstn_intrf\.clk ;
  wire \clk_rstn_intrf\.rst ;
  wire [15:0]data_i;
  wire [15:0]data_i_IBUF;
  wire [15:0]data_o;
  wire \data_o[0]_i_1_n_1 ;
  wire \data_o[10]_i_1_n_1 ;
  wire \data_o[11]_i_1_n_1 ;
  wire \data_o[12]_i_1_n_1 ;
  wire \data_o[13]_i_1_n_1 ;
  wire \data_o[14]_i_1_n_1 ;
  wire \data_o[15]_i_1_n_1 ;
  wire \data_o[15]_i_2_n_1 ;
  wire \data_o[1]_i_1_n_1 ;
  wire \data_o[2]_i_1_n_1 ;
  wire \data_o[3]_i_1_n_1 ;
  wire \data_o[4]_i_1_n_1 ;
  wire \data_o[5]_i_1_n_1 ;
  wire \data_o[6]_i_1_n_1 ;
  wire \data_o[7]_i_1_n_1 ;
  wire \data_o[8]_i_1_n_1 ;
  wire \data_o[9]_i_1_n_1 ;
  wire [15:0]data_o_OBUF;
  wire empty_o;
  wire empty_o_OBUF;
  wire full_o;
  wire full_o_OBUF;
  wire [2:0]items_count;
  wire \items_count[0]_i_1_n_1 ;
  wire \items_count[1]_i_1_n_1 ;
  wire \items_count[2]_i_1_n_1 ;
  wire \items_count[2]_i_2_n_1 ;
  wire \items_count_reg[2]_i_3_n_1 ;
  wire mem;
  wire \mem[1][15]_i_1_n_1 ;
  wire \mem[2][15]_i_1_n_1 ;
  wire [15:0]\mem_reg[0] ;
  wire [15:0]\mem_reg[1] ;
  wire [15:0]\mem_reg[2] ;
  wire n_0_32_BUFG;
  wire n_0_32_BUFG_inst_n_1;
  wire read_en_i;
  wire read_en_i_IBUF;
  wire \read_index[0]_i_1_n_1 ;
  wire \read_index[1]_i_1_n_1 ;
  wire \read_index_reg_n_1_[0] ;
  wire \read_index_reg_n_1_[1] ;
  wire write_en_i;
  wire write_en_i_IBUF;
  wire write_index;
  wire \write_index[0]_i_1_n_1 ;
  wire \write_index[1]_i_1_n_1 ;
  wire \write_index_reg_n_1_[0] ;
  wire \write_index_reg_n_1_[1] ;

  IBUF \data_i_IBUF[0]_inst 
       (.I(data_i[0]),
        .O(data_i_IBUF[0]));
  IBUF \data_i_IBUF[10]_inst 
       (.I(data_i[10]),
        .O(data_i_IBUF[10]));
  IBUF \data_i_IBUF[11]_inst 
       (.I(data_i[11]),
        .O(data_i_IBUF[11]));
  IBUF \data_i_IBUF[12]_inst 
       (.I(data_i[12]),
        .O(data_i_IBUF[12]));
  IBUF \data_i_IBUF[13]_inst 
       (.I(data_i[13]),
        .O(data_i_IBUF[13]));
  IBUF \data_i_IBUF[14]_inst 
       (.I(data_i[14]),
        .O(data_i_IBUF[14]));
  IBUF \data_i_IBUF[15]_inst 
       (.I(data_i[15]),
        .O(data_i_IBUF[15]));
  IBUF \data_i_IBUF[1]_inst 
       (.I(data_i[1]),
        .O(data_i_IBUF[1]));
  IBUF \data_i_IBUF[2]_inst 
       (.I(data_i[2]),
        .O(data_i_IBUF[2]));
  IBUF \data_i_IBUF[3]_inst 
       (.I(data_i[3]),
        .O(data_i_IBUF[3]));
  IBUF \data_i_IBUF[4]_inst 
       (.I(data_i[4]),
        .O(data_i_IBUF[4]));
  IBUF \data_i_IBUF[5]_inst 
       (.I(data_i[5]),
        .O(data_i_IBUF[5]));
  IBUF \data_i_IBUF[6]_inst 
       (.I(data_i[6]),
        .O(data_i_IBUF[6]));
  IBUF \data_i_IBUF[7]_inst 
       (.I(data_i[7]),
        .O(data_i_IBUF[7]));
  IBUF \data_i_IBUF[8]_inst 
       (.I(data_i[8]),
        .O(data_i_IBUF[8]));
  IBUF \data_i_IBUF[9]_inst 
       (.I(data_i[9]),
        .O(data_i_IBUF[9]));
  LUT5 #(
    .INIT(32'h0CFA0C0A)) 
    \data_o[0]_i_1 
       (.I0(\mem_reg[0] [0]),
        .I1(\mem_reg[2] [0]),
        .I2(\read_index_reg_n_1_[0] ),
        .I3(\read_index_reg_n_1_[1] ),
        .I4(\mem_reg[1] [0]),
        .O(\data_o[0]_i_1_n_1 ));
  LUT5 #(
    .INIT(32'h0CFA0C0A)) 
    \data_o[10]_i_1 
       (.I0(\mem_reg[0] [10]),
        .I1(\mem_reg[2] [10]),
        .I2(\read_index_reg_n_1_[0] ),
        .I3(\read_index_reg_n_1_[1] ),
        .I4(\mem_reg[1] [10]),
        .O(\data_o[10]_i_1_n_1 ));
  LUT5 #(
    .INIT(32'h0CFA0C0A)) 
    \data_o[11]_i_1 
       (.I0(\mem_reg[0] [11]),
        .I1(\mem_reg[2] [11]),
        .I2(\read_index_reg_n_1_[0] ),
        .I3(\read_index_reg_n_1_[1] ),
        .I4(\mem_reg[1] [11]),
        .O(\data_o[11]_i_1_n_1 ));
  LUT5 #(
    .INIT(32'h0CFA0C0A)) 
    \data_o[12]_i_1 
       (.I0(\mem_reg[0] [12]),
        .I1(\mem_reg[2] [12]),
        .I2(\read_index_reg_n_1_[0] ),
        .I3(\read_index_reg_n_1_[1] ),
        .I4(\mem_reg[1] [12]),
        .O(\data_o[12]_i_1_n_1 ));
  LUT5 #(
    .INIT(32'h0CFA0C0A)) 
    \data_o[13]_i_1 
       (.I0(\mem_reg[0] [13]),
        .I1(\mem_reg[2] [13]),
        .I2(\read_index_reg_n_1_[0] ),
        .I3(\read_index_reg_n_1_[1] ),
        .I4(\mem_reg[1] [13]),
        .O(\data_o[13]_i_1_n_1 ));
  LUT5 #(
    .INIT(32'h0CFA0C0A)) 
    \data_o[14]_i_1 
       (.I0(\mem_reg[0] [14]),
        .I1(\mem_reg[2] [14]),
        .I2(\read_index_reg_n_1_[0] ),
        .I3(\read_index_reg_n_1_[1] ),
        .I4(\mem_reg[1] [14]),
        .O(\data_o[14]_i_1_n_1 ));
  LUT5 #(
    .INIT(32'hAAA80000)) 
    \data_o[15]_i_1 
       (.I0(read_en_i_IBUF),
        .I1(items_count[1]),
        .I2(items_count[0]),
        .I3(items_count[2]),
        .I4(\items_count_reg[2]_i_3_n_1 ),
        .O(\data_o[15]_i_1_n_1 ));
  LUT5 #(
    .INIT(32'h0CFA0C0A)) 
    \data_o[15]_i_2 
       (.I0(\mem_reg[0] [15]),
        .I1(\mem_reg[2] [15]),
        .I2(\read_index_reg_n_1_[0] ),
        .I3(\read_index_reg_n_1_[1] ),
        .I4(\mem_reg[1] [15]),
        .O(\data_o[15]_i_2_n_1 ));
  LUT5 #(
    .INIT(32'h0CFA0C0A)) 
    \data_o[1]_i_1 
       (.I0(\mem_reg[0] [1]),
        .I1(\mem_reg[2] [1]),
        .I2(\read_index_reg_n_1_[0] ),
        .I3(\read_index_reg_n_1_[1] ),
        .I4(\mem_reg[1] [1]),
        .O(\data_o[1]_i_1_n_1 ));
  LUT5 #(
    .INIT(32'h0CFA0C0A)) 
    \data_o[2]_i_1 
       (.I0(\mem_reg[0] [2]),
        .I1(\mem_reg[2] [2]),
        .I2(\read_index_reg_n_1_[0] ),
        .I3(\read_index_reg_n_1_[1] ),
        .I4(\mem_reg[1] [2]),
        .O(\data_o[2]_i_1_n_1 ));
  LUT5 #(
    .INIT(32'h0CFA0C0A)) 
    \data_o[3]_i_1 
       (.I0(\mem_reg[0] [3]),
        .I1(\mem_reg[2] [3]),
        .I2(\read_index_reg_n_1_[0] ),
        .I3(\read_index_reg_n_1_[1] ),
        .I4(\mem_reg[1] [3]),
        .O(\data_o[3]_i_1_n_1 ));
  LUT5 #(
    .INIT(32'h0CFA0C0A)) 
    \data_o[4]_i_1 
       (.I0(\mem_reg[0] [4]),
        .I1(\mem_reg[2] [4]),
        .I2(\read_index_reg_n_1_[0] ),
        .I3(\read_index_reg_n_1_[1] ),
        .I4(\mem_reg[1] [4]),
        .O(\data_o[4]_i_1_n_1 ));
  LUT5 #(
    .INIT(32'h0CFA0C0A)) 
    \data_o[5]_i_1 
       (.I0(\mem_reg[0] [5]),
        .I1(\mem_reg[2] [5]),
        .I2(\read_index_reg_n_1_[0] ),
        .I3(\read_index_reg_n_1_[1] ),
        .I4(\mem_reg[1] [5]),
        .O(\data_o[5]_i_1_n_1 ));
  LUT5 #(
    .INIT(32'h0CFA0C0A)) 
    \data_o[6]_i_1 
       (.I0(\mem_reg[0] [6]),
        .I1(\mem_reg[2] [6]),
        .I2(\read_index_reg_n_1_[0] ),
        .I3(\read_index_reg_n_1_[1] ),
        .I4(\mem_reg[1] [6]),
        .O(\data_o[6]_i_1_n_1 ));
  LUT5 #(
    .INIT(32'h0CFA0C0A)) 
    \data_o[7]_i_1 
       (.I0(\mem_reg[0] [7]),
        .I1(\mem_reg[2] [7]),
        .I2(\read_index_reg_n_1_[0] ),
        .I3(\read_index_reg_n_1_[1] ),
        .I4(\mem_reg[1] [7]),
        .O(\data_o[7]_i_1_n_1 ));
  LUT5 #(
    .INIT(32'h0CFA0C0A)) 
    \data_o[8]_i_1 
       (.I0(\mem_reg[0] [8]),
        .I1(\mem_reg[2] [8]),
        .I2(\read_index_reg_n_1_[0] ),
        .I3(\read_index_reg_n_1_[1] ),
        .I4(\mem_reg[1] [8]),
        .O(\data_o[8]_i_1_n_1 ));
  LUT5 #(
    .INIT(32'h0CFA0C0A)) 
    \data_o[9]_i_1 
       (.I0(\mem_reg[0] [9]),
        .I1(\mem_reg[2] [9]),
        .I2(\read_index_reg_n_1_[0] ),
        .I3(\read_index_reg_n_1_[1] ),
        .I4(\mem_reg[1] [9]),
        .O(\data_o[9]_i_1_n_1 ));
  OBUF \data_o_OBUF[0]_inst 
       (.I(data_o_OBUF[0]),
        .O(data_o[0]));
  OBUF \data_o_OBUF[10]_inst 
       (.I(data_o_OBUF[10]),
        .O(data_o[10]));
  OBUF \data_o_OBUF[11]_inst 
       (.I(data_o_OBUF[11]),
        .O(data_o[11]));
  OBUF \data_o_OBUF[12]_inst 
       (.I(data_o_OBUF[12]),
        .O(data_o[12]));
  OBUF \data_o_OBUF[13]_inst 
       (.I(data_o_OBUF[13]),
        .O(data_o[13]));
  OBUF \data_o_OBUF[14]_inst 
       (.I(data_o_OBUF[14]),
        .O(data_o[14]));
  OBUF \data_o_OBUF[15]_inst 
       (.I(data_o_OBUF[15]),
        .O(data_o[15]));
  OBUF \data_o_OBUF[1]_inst 
       (.I(data_o_OBUF[1]),
        .O(data_o[1]));
  OBUF \data_o_OBUF[2]_inst 
       (.I(data_o_OBUF[2]),
        .O(data_o[2]));
  OBUF \data_o_OBUF[3]_inst 
       (.I(data_o_OBUF[3]),
        .O(data_o[3]));
  OBUF \data_o_OBUF[4]_inst 
       (.I(data_o_OBUF[4]),
        .O(data_o[4]));
  OBUF \data_o_OBUF[5]_inst 
       (.I(data_o_OBUF[5]),
        .O(data_o[5]));
  OBUF \data_o_OBUF[6]_inst 
       (.I(data_o_OBUF[6]),
        .O(data_o[6]));
  OBUF \data_o_OBUF[7]_inst 
       (.I(data_o_OBUF[7]),
        .O(data_o[7]));
  OBUF \data_o_OBUF[8]_inst 
       (.I(data_o_OBUF[8]),
        .O(data_o[8]));
  OBUF \data_o_OBUF[9]_inst 
       (.I(data_o_OBUF[9]),
        .O(data_o[9]));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[0] 
       (.C(n_0_32_BUFG),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(\data_o[0]_i_1_n_1 ),
        .Q(data_o_OBUF[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[10] 
       (.C(n_0_32_BUFG),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(\data_o[10]_i_1_n_1 ),
        .Q(data_o_OBUF[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[11] 
       (.C(n_0_32_BUFG),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(\data_o[11]_i_1_n_1 ),
        .Q(data_o_OBUF[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[12] 
       (.C(n_0_32_BUFG),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(\data_o[12]_i_1_n_1 ),
        .Q(data_o_OBUF[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[13] 
       (.C(n_0_32_BUFG),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(\data_o[13]_i_1_n_1 ),
        .Q(data_o_OBUF[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[14] 
       (.C(n_0_32_BUFG),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(\data_o[14]_i_1_n_1 ),
        .Q(data_o_OBUF[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[15] 
       (.C(n_0_32_BUFG),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(\data_o[15]_i_2_n_1 ),
        .Q(data_o_OBUF[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[1] 
       (.C(n_0_32_BUFG),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(\data_o[1]_i_1_n_1 ),
        .Q(data_o_OBUF[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[2] 
       (.C(n_0_32_BUFG),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(\data_o[2]_i_1_n_1 ),
        .Q(data_o_OBUF[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[3] 
       (.C(n_0_32_BUFG),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(\data_o[3]_i_1_n_1 ),
        .Q(data_o_OBUF[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[4] 
       (.C(n_0_32_BUFG),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(\data_o[4]_i_1_n_1 ),
        .Q(data_o_OBUF[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[5] 
       (.C(n_0_32_BUFG),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(\data_o[5]_i_1_n_1 ),
        .Q(data_o_OBUF[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[6] 
       (.C(n_0_32_BUFG),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(\data_o[6]_i_1_n_1 ),
        .Q(data_o_OBUF[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[7] 
       (.C(n_0_32_BUFG),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(\data_o[7]_i_1_n_1 ),
        .Q(data_o_OBUF[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[8] 
       (.C(n_0_32_BUFG),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(\data_o[8]_i_1_n_1 ),
        .Q(data_o_OBUF[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_o_reg[9] 
       (.C(n_0_32_BUFG),
        .CE(\data_o[15]_i_1_n_1 ),
        .D(\data_o[9]_i_1_n_1 ),
        .Q(data_o_OBUF[9]),
        .R(1'b0));
  OBUF empty_o_OBUF_inst
       (.I(empty_o_OBUF),
        .O(empty_o));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h01)) 
    empty_o_OBUF_inst_i_1
       (.I0(items_count[0]),
        .I1(items_count[1]),
        .I2(items_count[2]),
        .O(empty_o_OBUF));
  OBUF full_o_OBUF_inst
       (.I(full_o_OBUF),
        .O(full_o));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h08)) 
    full_o_OBUF_inst_i_1
       (.I0(items_count[0]),
        .I1(items_count[1]),
        .I2(items_count[2]),
        .O(full_o_OBUF));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h0455FFA8)) 
    \items_count[0]_i_1 
       (.I0(read_en_i_IBUF),
        .I1(items_count[1]),
        .I2(items_count[2]),
        .I3(write_en_i_IBUF),
        .I4(items_count[0]),
        .O(\items_count[0]_i_1_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hF00C7FA0)) 
    \items_count[1]_i_1 
       (.I0(write_en_i_IBUF),
        .I1(items_count[2]),
        .I2(items_count[0]),
        .I3(items_count[1]),
        .I4(read_en_i_IBUF),
        .O(\items_count[1]_i_1_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hCCC04CCC)) 
    \items_count[2]_i_1 
       (.I0(write_en_i_IBUF),
        .I1(items_count[2]),
        .I2(items_count[0]),
        .I3(items_count[1]),
        .I4(read_en_i_IBUF),
        .O(\items_count[2]_i_1_n_1 ));
  LUT1 #(
    .INIT(2'h1)) 
    \items_count[2]_i_2 
       (.I0(\items_count_reg[2]_i_3_n_1 ),
        .O(\items_count[2]_i_2_n_1 ));
  FDCE #(
    .INIT(1'b0)) 
    \items_count_reg[0] 
       (.C(n_0_32_BUFG),
        .CE(1'b1),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(\items_count[0]_i_1_n_1 ),
        .Q(items_count[0]));
  FDCE #(
    .INIT(1'b0)) 
    \items_count_reg[1] 
       (.C(n_0_32_BUFG),
        .CE(1'b1),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(\items_count[1]_i_1_n_1 ),
        .Q(items_count[1]));
  FDCE #(
    .INIT(1'b0)) 
    \items_count_reg[2] 
       (.C(n_0_32_BUFG),
        .CE(1'b1),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(\items_count[2]_i_1_n_1 ),
        .Q(items_count[2]));
  IBUF \items_count_reg[2]_i_3 
       (.I(\clk_rstn_intrf\.rst ),
        .O(\items_count_reg[2]_i_3_n_1 ));
  LUT3 #(
    .INIT(8'h04)) 
    \mem[0][15]_i_1 
       (.I0(\write_index_reg_n_1_[0] ),
        .I1(write_index),
        .I2(\write_index_reg_n_1_[1] ),
        .O(mem));
  LUT3 #(
    .INIT(8'h08)) 
    \mem[1][15]_i_1 
       (.I0(\write_index_reg_n_1_[0] ),
        .I1(write_index),
        .I2(\write_index_reg_n_1_[1] ),
        .O(\mem[1][15]_i_1_n_1 ));
  LUT3 #(
    .INIT(8'h40)) 
    \mem[2][15]_i_1 
       (.I0(\write_index_reg_n_1_[0] ),
        .I1(write_index),
        .I2(\write_index_reg_n_1_[1] ),
        .O(\mem[2][15]_i_1_n_1 ));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][0] 
       (.C(n_0_32_BUFG),
        .CE(mem),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[0]),
        .Q(\mem_reg[0] [0]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][10] 
       (.C(n_0_32_BUFG),
        .CE(mem),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[10]),
        .Q(\mem_reg[0] [10]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][11] 
       (.C(n_0_32_BUFG),
        .CE(mem),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[11]),
        .Q(\mem_reg[0] [11]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][12] 
       (.C(n_0_32_BUFG),
        .CE(mem),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[12]),
        .Q(\mem_reg[0] [12]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][13] 
       (.C(n_0_32_BUFG),
        .CE(mem),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[13]),
        .Q(\mem_reg[0] [13]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][14] 
       (.C(n_0_32_BUFG),
        .CE(mem),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[14]),
        .Q(\mem_reg[0] [14]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][15] 
       (.C(n_0_32_BUFG),
        .CE(mem),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[15]),
        .Q(\mem_reg[0] [15]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][1] 
       (.C(n_0_32_BUFG),
        .CE(mem),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[1]),
        .Q(\mem_reg[0] [1]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][2] 
       (.C(n_0_32_BUFG),
        .CE(mem),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[2]),
        .Q(\mem_reg[0] [2]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][3] 
       (.C(n_0_32_BUFG),
        .CE(mem),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[3]),
        .Q(\mem_reg[0] [3]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][4] 
       (.C(n_0_32_BUFG),
        .CE(mem),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[4]),
        .Q(\mem_reg[0] [4]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][5] 
       (.C(n_0_32_BUFG),
        .CE(mem),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[5]),
        .Q(\mem_reg[0] [5]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][6] 
       (.C(n_0_32_BUFG),
        .CE(mem),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[6]),
        .Q(\mem_reg[0] [6]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][7] 
       (.C(n_0_32_BUFG),
        .CE(mem),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[7]),
        .Q(\mem_reg[0] [7]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][8] 
       (.C(n_0_32_BUFG),
        .CE(mem),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[8]),
        .Q(\mem_reg[0] [8]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[0][9] 
       (.C(n_0_32_BUFG),
        .CE(mem),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[9]),
        .Q(\mem_reg[0] [9]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][0] 
       (.C(n_0_32_BUFG),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[0]),
        .Q(\mem_reg[1] [0]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][10] 
       (.C(n_0_32_BUFG),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[10]),
        .Q(\mem_reg[1] [10]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][11] 
       (.C(n_0_32_BUFG),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[11]),
        .Q(\mem_reg[1] [11]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][12] 
       (.C(n_0_32_BUFG),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[12]),
        .Q(\mem_reg[1] [12]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][13] 
       (.C(n_0_32_BUFG),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[13]),
        .Q(\mem_reg[1] [13]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][14] 
       (.C(n_0_32_BUFG),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[14]),
        .Q(\mem_reg[1] [14]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][15] 
       (.C(n_0_32_BUFG),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[15]),
        .Q(\mem_reg[1] [15]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][1] 
       (.C(n_0_32_BUFG),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[1]),
        .Q(\mem_reg[1] [1]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][2] 
       (.C(n_0_32_BUFG),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[2]),
        .Q(\mem_reg[1] [2]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][3] 
       (.C(n_0_32_BUFG),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[3]),
        .Q(\mem_reg[1] [3]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][4] 
       (.C(n_0_32_BUFG),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[4]),
        .Q(\mem_reg[1] [4]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][5] 
       (.C(n_0_32_BUFG),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[5]),
        .Q(\mem_reg[1] [5]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][6] 
       (.C(n_0_32_BUFG),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[6]),
        .Q(\mem_reg[1] [6]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][7] 
       (.C(n_0_32_BUFG),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[7]),
        .Q(\mem_reg[1] [7]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][8] 
       (.C(n_0_32_BUFG),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[8]),
        .Q(\mem_reg[1] [8]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[1][9] 
       (.C(n_0_32_BUFG),
        .CE(\mem[1][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[9]),
        .Q(\mem_reg[1] [9]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][0] 
       (.C(n_0_32_BUFG),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[0]),
        .Q(\mem_reg[2] [0]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][10] 
       (.C(n_0_32_BUFG),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[10]),
        .Q(\mem_reg[2] [10]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][11] 
       (.C(n_0_32_BUFG),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[11]),
        .Q(\mem_reg[2] [11]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][12] 
       (.C(n_0_32_BUFG),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[12]),
        .Q(\mem_reg[2] [12]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][13] 
       (.C(n_0_32_BUFG),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[13]),
        .Q(\mem_reg[2] [13]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][14] 
       (.C(n_0_32_BUFG),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[14]),
        .Q(\mem_reg[2] [14]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][15] 
       (.C(n_0_32_BUFG),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[15]),
        .Q(\mem_reg[2] [15]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][1] 
       (.C(n_0_32_BUFG),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[1]),
        .Q(\mem_reg[2] [1]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][2] 
       (.C(n_0_32_BUFG),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[2]),
        .Q(\mem_reg[2] [2]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][3] 
       (.C(n_0_32_BUFG),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[3]),
        .Q(\mem_reg[2] [3]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][4] 
       (.C(n_0_32_BUFG),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[4]),
        .Q(\mem_reg[2] [4]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][5] 
       (.C(n_0_32_BUFG),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[5]),
        .Q(\mem_reg[2] [5]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][6] 
       (.C(n_0_32_BUFG),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[6]),
        .Q(\mem_reg[2] [6]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][7] 
       (.C(n_0_32_BUFG),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[7]),
        .Q(\mem_reg[2] [7]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][8] 
       (.C(n_0_32_BUFG),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[8]),
        .Q(\mem_reg[2] [8]));
  FDCE #(
    .INIT(1'b0)) 
    \mem_reg[2][9] 
       (.C(n_0_32_BUFG),
        .CE(\mem[2][15]_i_1_n_1 ),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(data_i_IBUF[9]),
        .Q(\mem_reg[2] [9]));
  BUFG n_0_32_BUFG_inst
       (.I(n_0_32_BUFG_inst_n_1),
        .O(n_0_32_BUFG));
  IBUF n_0_32_BUFG_inst_i_1
       (.I(\clk_rstn_intrf\.clk ),
        .O(n_0_32_BUFG_inst_n_1));
  IBUF read_en_i_IBUF_inst
       (.I(read_en_i),
        .O(read_en_i_IBUF));
  LUT6 #(
    .INIT(64'hAAABFFFF55540000)) 
    \read_index[0]_i_1 
       (.I0(\read_index_reg_n_1_[1] ),
        .I1(items_count[2]),
        .I2(items_count[0]),
        .I3(items_count[1]),
        .I4(read_en_i_IBUF),
        .I5(\read_index_reg_n_1_[0] ),
        .O(\read_index[0]_i_1_n_1 ));
  LUT6 #(
    .INIT(64'h0003FFFFAAA80000)) 
    \read_index[1]_i_1 
       (.I0(\read_index_reg_n_1_[0] ),
        .I1(items_count[2]),
        .I2(items_count[0]),
        .I3(items_count[1]),
        .I4(read_en_i_IBUF),
        .I5(\read_index_reg_n_1_[1] ),
        .O(\read_index[1]_i_1_n_1 ));
  FDCE #(
    .INIT(1'b0)) 
    \read_index_reg[0] 
       (.C(n_0_32_BUFG),
        .CE(1'b1),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(\read_index[0]_i_1_n_1 ),
        .Q(\read_index_reg_n_1_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \read_index_reg[1] 
       (.C(n_0_32_BUFG),
        .CE(1'b1),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(\read_index[1]_i_1_n_1 ),
        .Q(\read_index_reg_n_1_[1] ));
  IBUF write_en_i_IBUF_inst
       (.I(write_en_i),
        .O(write_en_i_IBUF));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB4)) 
    \write_index[0]_i_1 
       (.I0(\write_index_reg_n_1_[1] ),
        .I1(write_index),
        .I2(\write_index_reg_n_1_[0] ),
        .O(\write_index[0]_i_1_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h38)) 
    \write_index[1]_i_1 
       (.I0(\write_index_reg_n_1_[0] ),
        .I1(write_index),
        .I2(\write_index_reg_n_1_[1] ),
        .O(\write_index[1]_i_1_n_1 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h01BF0000)) 
    \write_index[1]_i_2 
       (.I0(items_count[2]),
        .I1(items_count[0]),
        .I2(items_count[1]),
        .I3(read_en_i_IBUF),
        .I4(write_en_i_IBUF),
        .O(write_index));
  FDCE #(
    .INIT(1'b0)) 
    \write_index_reg[0] 
       (.C(n_0_32_BUFG),
        .CE(1'b1),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(\write_index[0]_i_1_n_1 ),
        .Q(\write_index_reg_n_1_[0] ));
  FDCE #(
    .INIT(1'b0)) 
    \write_index_reg[1] 
       (.C(n_0_32_BUFG),
        .CE(1'b1),
        .CLR(\items_count[2]_i_2_n_1 ),
        .D(\write_index[1]_i_1_n_1 ),
        .Q(\write_index_reg_n_1_[1] ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
